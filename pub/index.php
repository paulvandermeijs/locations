<?php
include "autoload.php";

/**
 * @var array
 */
$request = (function () {
    // Parse request
    $request = parse_url($_SERVER["REQUEST_URI"]);

    // Get controller, action and id from path
    list($controller, $action, $id) = (function () use ($request) {
        $trimmedPath = trim($request["path"], "/");
        
        $parts = $trimmedPath ? explode("/", $trimmedPath, 3) : [];
        
        return $parts + [null, null, null];
    })();

    // Set controller, action and id on request
    $request["controller"] = $controller ?? $_GET["controller"] ?? "Home";
    $request["action"]     = $action ?? $_GET["action"] ?? "index";
    $request["id"]         = $id ?? $_GET["id"] ?? null;
    
    return $request;
})();

$controller = (function () use ($request) {
    /**
     * The name of the requested controller.
     *
     * @var string
     */
    $controllerName = "Controller\\{$request["controller"]}Controller";

    // Check if controller exists
    if (!class_exists($controllerName)) {
        exit("Unknown controller");
    }

    /**
     * @var ReflectionClass
     */
    $reflection = new ReflectionClass($controllerName);

    // Check if action exists on controller
    if (!$reflection->hasMethod($request["action"])) {
        exit("Unknown action");
    }

    // Create controller
    return new $controllerName($request);
})();

try {
    
    // Print result from action
    echo $controller->{$request["action"]}($request["id"]);
    
} catch (Lib\NotFoundException $e) {
    
    // Send not found header on exception
    header("HTTP/1.0 404 Not Found");
    
    exit;
    
} catch (Exception $e) {
    
    echo $e->getMessage();
    
}
