<?php
/**
 * @package Model
 *
 * @author Paul van der Meijs <paulvandermeijs@gmail.com>
 * @copyright Copyright (c) 2018, Paul van der Meijs
 *
 * @version 0.0.1
 */

namespace Model;

/**
 * Location
 * ========
 *
 * Represents a single location.
 */
class Location
{
    
    /**
     * @var string
     */
    private $name;

    /**
     * @var float
     */
    private $latitude;
    
    /**
     * @var float
     */
    private $longitude;
    
    /**
     * @var string
     */
    private $street;
    
    public function setName(string $name) : Location
    {
        $this->name = $name;
        
        return $this;
    }
    
    public function setLatitude(string $latitude) : Location
    {
        $this->latitude = $latitude;
        
        return $this;
    }
    
    public function setLongitude(string $longitude) : Location
    {
        $this->longitude = $longitude;
        
        return $this;
    }
    
    public function setStreet(string $street) : Location
    {
        $this->street = $street;
        
        return $this;
    }
    
    public function __toString() : string
    {
        return json_encode((object)[
            "name"      => $this->name,
            "latitude"  => $this->latitude,
            "longitude" => $this->longitude,
            "street"    => $this->street,
        ]);
    }
    
    /**
     * Create a new location from given attributes.
     * 
     * @param string $name
     * @param float $latitude
     * @param float $longitude
     * @param string $street
     * @return Locations
     */
    public static function createFromAttributes(
        string $name, 
        float $latitude, 
        float $longitude, 
        string $street
    ) : Location
    {
        return (new Location)->setName($name)
                             ->setLatitude($latitude)
                             ->setLongitude($longitude)
                             ->setStreet($street);
    }
    
    public static function createFromCsv(array $line) : Location
    {
        [$name, $latitude, $longitude, $street] = $line;
        
        return static::createFromAttributes(
            (string)$name, 
            (float)$latitude, 
            (float)$longitude, 
            (string)$street
        );
    }
    
}
