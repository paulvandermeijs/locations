<?php
/**
 * @package Model
 *
 * @author Paul van der Meijs <paulvandermeijs@gmail.com>
 * @copyright Copyright (c) 2018, Paul van der Meijs
 *
 * @version 0.0.1
 */

namespace Model;

/**
 * Location Collection
 * ===================
 *
 * A collection of locations.
 */
class LocationCollection implements
    \IteratorAggregate, 
    \ArrayAccess, 
    \Countable
{
    
    /**
     * @var array
     */
    private $locations;
    
    public static function createFromFile($path) : LocationCollection
    {
        if (!\file_exists($path)) {
            throw new \Exception("File not found");
        }
        
        $file = fopen($path, "r");
        
        $locations = new static;
        
        while (false !== ($line = fgetcsv($file))) {
            $locations[] = Location::createFromCsv($line);
        }
        
        fclose($file);
        
        return $locations;
    }
    
    /*
     * ArrayIterator
     */
     
    public function getIterator() : \ArrayIterator
    {
        return new \ArrayIterator($this->locations);
    }

    /*
     * ArrayAccess
     */

    public function offsetExists($offset) : bool
    {
        return isset($this->locations[$offset]);
    }

    public function offsetGet($offset)
    {
        return isset($this->locations[$offset]) ? $this->locations[$offset] : null;
    }

    public function offsetSet($offset, $value) : void
    {
        if (!($value instanceof Location)) {
            throw new \Exception("Value must be of type Location");
        }
        
        if (is_null($offset)) {
            $this->locations[] = $value;
        } else {
            $this->locations[$offset] = $value;
        }
    }

    public function offsetUnset($offset) : void
    {
        unset($this->locations[$offset]);
    }

    /*
     * Countable
     */

    public function count() : int
    {
        return count($this->locations);
    }
    
}
