<?php
/**
 * @package Lib
 *
 * @author Paul van der Meijs <paulvandermeijs@gmail.com>
 * @copyright Copyright (c) 2018, Paul van der Meijs
 *
 * @version 0.0.1
 */

namespace Lib;

/**
 * HTTP Response
 * =============
 *
 * Use to return a HTTP response from a controller.
 */
class HttpResponse
{
    
    private $responseString;
    
    public function __construct(string $responseString)
    {
        $this->responseString = $responseString;
    }
    
    public function __toString() : string
    {
        return (string)$this->responseString;
    }
    
}
