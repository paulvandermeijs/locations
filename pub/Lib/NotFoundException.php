<?php
/**
 * @package Lib
 *
 * @author Paul van der Meijs <paulvandermeijs@gmail.com>
 * @copyright Copyright (c) 2018, Paul van der Meijs
 *
 * @version 0.0.1
 */

namespace Lib;

/**
 * Not Found Exception
 * ===================
 *
 * This exception is to be used when requested content isn't found.
 */
class NotFoundException extends \Exception
{
    
}
