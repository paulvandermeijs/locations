<?php
/**
 * @package Controller
 *
 * @author Paul van der Meijs <paulvandermeijs@gmail.com>
 * @copyright Copyright (c) 2018, Paul van der Meijs
 *
 * @version 0.0.1
 */

namespace Controller;

/**
 * Home Controller
 * ===============
 *
 * The default controller.
 */
class HomeController
{
    
    public function index() : string
    {
        return "Hello, World!";
    }
    
}
