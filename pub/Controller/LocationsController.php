<?php
/**
 * @package Controller
 *
 * @author Paul van der Meijs <paulvandermeijs@gmail.com>
 * @copyright Copyright (c) 2018, Paul van der Meijs
 *
 * @version 0.0.1
 */

namespace Controller;

use Model\LocationCollection;

use Lib\{
    HttpResponse,
    NotFoundException
};

/**
 * Locations Controller
 * ====================
 *
 * Show locations from file.
 */
class LocationsController
{
    
    private $file = "data/locations.csv";
    
    public function index(?int $id = null) : HttpResponse
    {
        try {
            $locationCollection = LocationCollection::createFromFile($this->file);
        } catch (\Exception $e) {
            throw new \Exception("{$this->file} not found");
        }
        
        if (!isset($locationCollection[$id])) {
            throw new NotFoundException;
        }
        
        return new HttpResponse((string)$locationCollection[$id]);
    }
    
}
